Name: faircmakemodules
Version: 1.0.0
Release: 1%{?dist}
Summary: CMake Modules developed in the context of various FAIR projects
License: LGPLv3
%define github github.com
%define gh_org FairRootGroup
%define gh_repo FairCMakeModules
URL: https://fairrootgroup.github.io/%{gh_repo}
Source0: https://%{github}/%{gh_org}/%{gh_repo}/archive/v%{version}.tar.gz
BuildArch: noarch

BuildRequires: cmake
BuildRequires: gcc-c++

%description
CMake Modules developed in the context of and used by various FAIR projects.

%global debug_package %{nil}

%prep
%autosetup -n %{gh_repo}-%{version}

%build
%define builddir build
cmake -S. -B%{builddir} \
      -DCMAKE_INSTALL_PREFIX=%{_prefix} \
      -DCMAKE_BUILD_TYPE=Release
cmake --build %{builddir} %{?_smp_mflags}

%install
DESTDIR=%{buildroot} cmake --build build --target install
rm -rf %{buildroot}/%{_datadir}/doc

%files
%doc CHANGELOG.rst
%doc README.md
%license LICENSE
%{_datadir}/%{name}
%dir %{_datadir}/cmake
%{_datadir}/cmake/%{gh_repo}-%{version}


%changelog
* Thu Sep 9 2021 Dennis Klein <d.klein@gsi.de> - 1.0.0-1
- Package 1.0.0
* Mon May 24 2021 Dennis Klein <d.klein@gsi.de> - 0.2.0-1
- Package 0.2.0
* Thu May 13 2021 Dennis Klein <d.klein@gsi.de> - 0.1.0-1
- Package 0.1.0
